if not WarBoard_TogglerRes then WarBoard_TogglerRes = {} end
local WarBoard_TogglerRes = WarBoard_TogglerRes
local modName = "WarBoard_TogglerRes"
local modLabel = "Res"
local LabetSetTextColor = LabelSetTextColor
local Tooltips = Tooltips

function WarBoard_TogglerRes.Init()
	if LibWBToggler.CreateToggler(modName, modLabel, "resicon", 0, 0, nil, 0.75) then
		WindowSetDimensions(modName, 110, 30)
		WindowSetDimensions(modName.."Label", 78, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonDown", "WarBoard_TogglerRes.ToggleActive")
		LibWBToggler.RegisterEvent(modName, "OnRButtonDown", "Res.ShowOptions")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerRes.ShowStatus")
		if not Res.Enabled then LabelSetTextColor(modName.."Label", 255, 0, 0) else LabelSetTextColor(modName.."Label", 0, 255, 0) end
	end
end

function WarBoard_TogglerRes.ToggleActive()
	Res.Toggle()
	if Res.Enabled then LabelSetTextColor(modName.."Label", 0, 255, 0) else LabelSetTextColor(modName.."Label", 255, 0, 0) end
	WarBoard_TogglerRes.ShowStatus()
end

function WarBoard_TogglerRes.ShowStatus()
	Tooltips.CreateTextOnlyTooltip(modName, nil)
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor(modName))
	Tooltips.SetTooltipText(1, 1, L"Res Toggler")
	if Res.Enabled then Tooltips.SetTooltipColor(1, 1, 0, 255, 0) else Tooltips.SetTooltipColor(1, 1, 255, 0, 0) end
	Tooltips.SetTooltipText(2, 1, L"Left Click:")
	Tooltips.SetTooltipText(2, 3, L"Toggle Active")
	Tooltips.SetTooltipText(3, 1, L"Right Click:")
	Tooltips.SetTooltipText(3, 3, L"Toggle GUI")
	Tooltips.Finalize()
end
