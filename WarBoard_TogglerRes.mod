<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WarBoard_TogglerRes" version="0.1" date="13/1/2010" >
		<Author name="Medikage" email="medikage@gmail.com" />
		<Description text="Res Toggler module created to interact with LibWBToggler." />
		<VersionSettings gameVersion="1.3.4" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibWBToggler" />
			<Dependency name="Res" forceEnable="false" />
		</Dependencies>
		<Files>
			<File name="WarBoard_TogglerRes.xml" />
			<File name="WarBoard_TogglerRes.lua" />
		</Files>
		<OnInitialize>
			<CallFunction name="WarBoard_TogglerRes.Init" />
		</OnInitialize>
		<WARInfo>
			<Categories>
			    <!-- Whilst I realise these are wrong, there is a current bug
					 within warhammer that puts them all out by 1, so in game, 
					 these categories actually align properly. 
				-->
				<Category name="MAIL"/> 
				<Category name="COMBAT"/>
				<Category name="QUESTS"/>
			</Categories>
			<Careers>
				<Career name="DISCIPLE" />
				<Career name="RUNE_PRIEST" />
				<Career name="SHAMAN" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="ZEALOT" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>